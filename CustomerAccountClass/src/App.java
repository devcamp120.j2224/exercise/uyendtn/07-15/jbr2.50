public class App {
    public static void main(String[] args) throws Exception {
       //4. tạo 2 customer và in ra system out
       Customer customer1 =new Customer(1, "OHMM", 10);
       Customer customer2 =new Customer(2, "XTRA", 20);
       System.out.println(customer1.toString());
       System.out.println(customer2.toString());

       //5.tạo 2 account và in ra system out
       Account account1 = new Account(1, customer1, 100);
       Account account2 = new Account(2, customer2, 200);
       account1.deposit(50);
       account2.withdraw(100);
       System.out.println(account1.toString());
       System.out.println(account2.toString());
    }
}
